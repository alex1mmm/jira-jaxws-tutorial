package ru.matveev.alexey.jaxws.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class MyServiceLogHandler implements SOAPHandler<SOAPMessageContext> {
    private static final Logger log = LoggerFactory.getLogger(MyServiceLogHandler.class);

    @Override
    public Set<QName> getHeaders() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void close(MessageContext arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean handleFault(SOAPMessageContext arg0) {
        SOAPMessage message= arg0.getMessage();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            message.writeTo(out);
            String strMsg = new String(out.toByteArray());
            log.debug(String.format("handleFault message: ", strMsg));
        } catch (SOAPException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext arg0) {
        SOAPMessage message= arg0.getMessage();
        boolean isOutboundMessage=  (Boolean)arg0.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if(isOutboundMessage){
            log.debug("OUTBOUND MESSAGE\n");

        }else{
            log.debug("INBOUND MESSAGE\n");

        }
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            message.writeTo(out);
            String strMsg = new String(out.toByteArray());
            log.debug(String.format("handleFault message: %s", strMsg));

        } catch (SOAPException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
