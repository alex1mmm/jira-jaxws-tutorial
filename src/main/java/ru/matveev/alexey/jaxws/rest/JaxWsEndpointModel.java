package ru.matveev.alexey.jaxws.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class JaxWsEndpointModel {

    @XmlElement(name = "value")
    private String message;

    public JaxWsEndpointModel() {
    }

    public JaxWsEndpointModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}