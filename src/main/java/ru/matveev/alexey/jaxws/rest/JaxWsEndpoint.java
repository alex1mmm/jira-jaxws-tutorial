package ru.matveev.alexey.jaxws.rest;


import net.gcomputer.webservices.Dilbert;
import net.gcomputer.webservices.DilbertSoap;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import ru.matveev.alexey.jaxws.impl.MyServiceLogHandler;

/**
 * A resource of message.
 */
@Path("/service")
public class JaxWsEndpoint {

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response callEndpoint()
    {
        Dilbert dilbert = new Dilbert();
        DilbertSoap ss = dilbert.getDilbertSoap();
        BindingProvider bindProv = (BindingProvider) ss;
        java.util.List<Handler> handlers = bindProv.getBinding().getHandlerChain();
        handlers.add(new MyServiceLogHandler());
        bindProv.getBinding().setHandlerChain(handlers);
        String res = ss.todaysDilbert();
       return Response.ok(new JaxWsEndpointModel(res)).build();
    }


}